// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Это способ выполнить асинхронный запрос без перезагрузки страницы. Прозволяет сэкономить трафик, скорость отклика быстрее

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.
// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

function movies(arr, arr2) {
  let ulElem = document.createElement("ul");
  arr.forEach((obj) => {
    let liElem1 = document.createElement("li");
    liElem1.innerText = obj.episodeId;

    let liElem2 = document.createElement("li");
    liElem2.innerText = obj.name;

    let liElem3 = document.createElement("li");
    liElem3.innerText = obj.openingCrawl;

    let ulElem2 = document.createElement("ul");

    const newArr = arr2.filter((people) => {
      return obj.characters.includes(people.url);
    });

    characters(newArr, ulElem2);
    ulElem.append(liElem1, liElem2, liElem3, ulElem2);
  });

  document.body.append(ulElem);
}

function characters(arr2, perent) {
  arr2.forEach((obj) => {
    let liElem4 = document.createElement("li");
    liElem4.innerText = obj.name;
    perent.append(liElem4);
  });
}

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    console.log("Data:", data);
    fetch("https://ajax.test-danit.com/api/swapi/people/")
      .then((response) => {
        return response.json();
      })
      .then((people) => {
        console.log("Characters:", people);
        movies(data, people);
      });
  });
